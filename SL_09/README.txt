Team: Star Legends

Samuel Davis 	(101093250): In charge of programming, sound design and putting most of the 
		actual gameplay together.
Liam Cappello 	(101622876): In charge of all the visual sprites used in the game and UI.
Thomas Nguyen 	(101300688): In charge of organisination/management and UI design.

Gameplay:
You start in hyperspace on your away to a new system that is underattck from an evil force 
and it is your job to get in there and help as many people as you can.

The way of doing this is to visit planets as shown by the pointers on the edge of the screen.
These pointers will also tell the population of the planet your visiting:
-Bright yellow: This indicates a star and is not much value other than damaging your ship, and
		 don't worry, we turned on the safely gate.
-Red: Means there is much higher population here.
-Dark yellow/orange: Means a moderate amount of people.
-Green: Means a lower population.

Note: A pointer to a planet disapears when the planet is on your screen.

The way you gather score is once you have landed on the planet you get 1 point per-second
times by how populated the planet is (as seen in the pointers above). For example a planet
that has a red pointer may give x8-10 the score while a green can only be x1-3 times, so
keep an eye out for those red planets.

However, while you are landed on a planet and gaining score the population of the planet
will decrease based on the amount of seconds you where landed (while you are landed the 
population stays the same) and if you land there again you'll find it gives less of a 
multiplier. 
EG: You land on 10 planet (red) for 8 seconds and move away from the planet, you will find 
that the marker has turned green as the multiplier is now only 2.

Controls (mouse and keyboard only):
W, A, S, D: move the ship around in different directions.
(hold) space + W, A, S, D: doubles the amount of force to your ships movement.
(mouse) left-click: fires your weapon at the direction your ship is facing, keep in mind your
		ship maybe a little behind where you aim with your curser.
J: Take off into hyperspace and score your points! Keep in mind it takes a while to charge
		and you can still be damaged (and killed!) while it is charging.
L: Land on a landing pad on a planet and start gathering score. Just be ready to move as 
		enemies will still attack (and kill) you as your siting on landing pad
		helplessly. Press "L" again to regain control.

Resources/tools used to create this game:
Unity
Visual studio
Paint.net
Wavepad audio editor
Slack
Trello
SourceTree
Pinterest
Aseprite
Photoshop
https://freesound.org/

These are the specific links used for the differrent sounds in the game:
https://freesound.org/people/sharesynth/sounds/341230/
https://freesound.org/people/gamer127/sounds/463067/
https://freesound.org/people/Andrewkn/sounds/404458/
https://freesound.org/people/Santiboada/sounds/348027/
https://freesound.org/people/Assimulation_Gaming/sounds/330402/
https://freesound.org/people/k2tr/sounds/322970/
https://freesound.org/people/FoolBoyMedia/sounds/231254/
https://freesound.org/people/sharesynth/sounds/341244/
https://freesound.org/people/sharesynth/sounds/341255/
https://freesound.org/people/sharesynth/sounds/344506/
https://freesound.org/people/sharesynth/sounds/344507/
https://freesound.org/people/sharesynth/sounds/344510/
https://freesound.org/people/sharesynth/sounds/344511/
https://freesound.org/people/sharesynth/sounds/344512/
https://freesound.org/people/sharesynth/sounds/344513/

