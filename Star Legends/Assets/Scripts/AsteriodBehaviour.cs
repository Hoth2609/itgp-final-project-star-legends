﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteriodBehaviour : AbstractLifeSpan
{
    //TODO Add basic movement and destruction

    //TODO Add split behaviour

    private Rigidbody2D myRigidbody;
    private SpriteRenderer fSpriteRender;
    private EnumSize fSize;
    private EnumSize[] fSizes;
    private float[] fHitBoxSize;
    
    [SerializeField]
    private int fSpeed;
    [SerializeField]
    private Sprite[] fSprites;
    [SerializeField]
    private GameObject fAsteriod;

	// Use this for initialization
	void Awake ()
    {
        myRigidbody = GetComponent<Rigidbody2D>();
        fSpriteRender = GetComponent<SpriteRenderer>();

        fHitBoxSize = new float[] { 0.63f, 1.035f, 1.26f };
        fSizes = new EnumSize[] { EnumSize.Small, EnumSize.Medium, EnumSize.Large };
    }

    public void SetSize(int pSize = 0)
    {
        fSize = fSizes[pSize];
        fSpriteRender.sprite = fSprites[pSize];
        GetComponent<CircleCollider2D>().radius = fHitBoxSize[pSize];
    }

    private void Destory()
    {
        GameObject lAsteriod;
        int lSize = 0;

        GameObject.Find("GameController").GetComponent<AudioController>().PlayerAudio("explosion");

        switch (fSize)
        {
            case EnumSize.Small:
                lSize = -1;
                break;
            
            case EnumSize.Medium:
                lSize = 0;
                break;
           
            case EnumSize.Large:
                lSize = 1;
                break;
        }

        if (lSize >= 0)
        {
            lAsteriod = Instantiate(fAsteriod);
            lAsteriod.GetComponent<AsteriodBehaviour>().SetSize(lSize);
            lAsteriod.GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(-100, 101), Random.Range(-100, 101)));

            lAsteriod = Instantiate(fAsteriod);
            lAsteriod.GetComponent<AsteriodBehaviour>().SetSize(lSize);
            lAsteriod.GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(-100, 101), Random.Range(-100, 101)));
        }
    }

    private void OnTriggerEnter2D(Collider2D pCollision)
    {
        if (pCollision.tag == "Star" || pCollision.tag == "Planet")
            GameObject.Destroy(gameObject);

        if (pCollision.tag == "PlayerBullet")
        {
            Destory();
            GameObject.Destroy(gameObject);
        }
    }

    public void HitPlayer()
    {
        Vector2 lPlayerPos = GameObject.Find("Player").transform.position;

        Vector2 lAim = (lPlayerPos - (Vector2)transform.position);

        lAim = lAim.normalized;

        myRigidbody.velocity = lAim * fSpeed;
    }
}

