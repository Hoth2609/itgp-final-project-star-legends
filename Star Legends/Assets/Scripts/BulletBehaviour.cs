﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehaviour : AbstractLifeSpan
{
    private void OnTriggerEnter2D(Collider2D pCollision)
    {
        if (pCollision.tag != "Planet")
            GameObject.Destroy(gameObject);
    }
}