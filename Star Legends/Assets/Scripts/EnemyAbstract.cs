﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyAbstract : MonoBehaviour
{
    protected Rigidbody2D myRigidbody;
    [SerializeField]
    protected int fHealth;
    [SerializeField]
    protected int fMaxSpeed;
    [SerializeField]
    protected int fMaxForce;
    [SerializeField]
    protected int fBulletSpeed;
    [SerializeField]
    protected int fShootIntervil;
    [SerializeField]
    protected float fAggroRange;
    [SerializeField]
    protected GameObject fBullet;
    [SerializeField]
    protected GameObject fSatillite;
    [SerializeField]
    protected GameObject fAudio;

    protected int fMaxPresence = 10;
    protected GameObject fBestPlanet;
    protected Coroutine fAction;
    protected Coroutine fDamage;
    protected Sprite[] fDestroyStates;
    protected List<GameObject> fTroopsAvailable;
    protected Vector2 fAimPos;
    protected Vector2 fWanderTarget;

    protected abstract void Death();
    protected abstract IEnumerator Attack();

    protected virtual IEnumerator Movement()
    {
        Vector2 lPosTarget;
        Vector2 lForce;

        if (Vector2Distance(transform.position, fAimPos) < 10)
            DeployTroops();

        if (!GameController.gGameState)
            lForce = Wander();

        else if (fTroopsAvailable.Count > 0)
        {
            fBestPlanet = GameController.gWorldPlanets[0];
            PlanetController lBPControl = fBestPlanet.GetComponent<PlanetController>();

            foreach (GameObject p in GameController.gWorldPlanets)
            {
                PlanetController lpC = p.GetComponent<PlanetController>();
                if (lpC.EnemyPresence > lBPControl.EnemyPresence && lpC.EnemyPresence <= fMaxPresence)
                    fBestPlanet = p;
            }

            //Picks a random point within the selected planet
            lPosTarget = new Vector2(Random.Range(fBestPlanet.transform.position.x - lBPControl.PlanetRadius, fBestPlanet.transform.position.x + lBPControl.PlanetRadius), Random.Range(fBestPlanet.transform.position.y - lBPControl.PlanetRadius, fBestPlanet.transform.position.y + lBPControl.PlanetRadius));


            fAimPos = lPosTarget;
            lForce = Seek(lPosTarget);
        }

        else
        {
            Vector2 lPlayerPos = GameController.gPlayer.transform.position;
            transform.up = lPlayerPos.normalized;
            lForce = Seek(lPlayerPos);
        }

        lForce = Truncate(lForce, fMaxForce);
        myRigidbody.AddForce(lForce);
        myRigidbody.velocity = Truncate(myRigidbody.velocity, fMaxSpeed);
        transform.up = (myRigidbody.velocity).normalized;

        fAction = null;

        yield break;     
    }

    protected Vector2 Truncate(Vector2 vec2, float maxLength)
    {
        if (Vector2Length(vec2) > maxLength)
        {
            vec2.Normalize();
            vec2 *= maxLength;
        }

        return vec2;
    }

    protected float Vector2Length(Vector2 vec2)
    {
        //Debug.Log("Length" + (Mathf.Sqrt((vec2.x * vec2.x) + (vec2.y * vec2.y))));
        return Mathf.Sqrt((vec2.x * vec2.x) + (vec2.y * vec2.y));
    }

    protected float Vector2Distance(Vector2 vec1, Vector2 vec2)
    {
        float dx = vec2.x - vec1.x;
        float dy = vec2.y - vec1.y;
        return Mathf.Sqrt(dx * dx + dy * dy);
    }

    protected Vector2 Seek(Vector2 pTarget)
    {
        // seek
        Vector2 lTarget = pTarget;
        lTarget = (lTarget - (Vector2)transform.position).normalized * fMaxSpeed;
        lTarget = lTarget - myRigidbody.velocity;

        return lTarget;
    }

    protected Vector2 Wander()
    {
        Vector2 lTarget = fWanderTarget;

        int x = Random.Range(-1, 2);
        int y = Random.Range(-1, 2);

        lTarget += new Vector2(x, y);
        lTarget.Normalize();

        lTarget *= 5;

        // seek
        return (Seek(lTarget));
    }

    protected IEnumerator Damage()
    {
        fHealth--;

        if (fHealth <= 0)
        {
            GameObject.Find("GameController").GetComponent<AudioController>().PlayerAudio("explosion");
            Death();
            yield break;
        }

        yield return new WaitForSeconds(1);

        fDamage = null;
    }

    public void AddTroop(GameObject pUnit)
    {
        fTroopsAvailable.Add(pUnit);
    }

    protected void DeployTroops()
    {
        if (fBestPlanet != null)
        {
            while (fTroopsAvailable.Count > 0 && fBestPlanet.GetComponent<PlanetController>().EnemyPresence < fMaxPresence)
            {
                GameObject troop = fTroopsAvailable[0];
                if (troop == fSatillite && fBestPlanet.GetComponent<PlanetController>().HasSatillite)
                    fTroopsAvailable.RemoveAt(0);

                else
                {
                    GameObject lInstTroop = Instantiate(fTroopsAvailable[0]);
                    lInstTroop.transform.position = fAimPos;

                    fTroopsAvailable.RemoveAt(0);
                }
            }
        }       
    }
}
