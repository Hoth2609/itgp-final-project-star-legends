﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class PointerInfo : MonoBehaviour
{
    private Vector2 fPointPosition;
    private GameObject fPlanetPointer;
    [SerializeField]
    private Sprite[] fPointerSkins;

    private void Update()
    {
        if (fPointerSkins.Count() > 0 && fPlanetPointer != null)
        {
            if (fPlanetPointer.tag == "Star")
                GetComponent<SpriteRenderer>().sprite = fPointerSkins[0];
            else if (fPlanetPointer.GetComponent<PlanetController>().ScoreMax <= 3)
                GetComponent<SpriteRenderer>().sprite = fPointerSkins[1];
            else if (fPlanetPointer.GetComponent<PlanetController>().ScoreMax > 7)
                GetComponent<SpriteRenderer>().sprite = fPointerSkins[3];
            else
                GetComponent<SpriteRenderer>().sprite = fPointerSkins[2];

            if (PlanetPointer.GetComponent<SpriteRenderer>().isVisible)
                GetComponent<SpriteRenderer>().enabled = false;
            else
                GetComponent<SpriteRenderer>().enabled = true;
        }     
    }

    public Vector2 PointPosition
    {
        get
        {
            return fPointPosition;
        }
    }

    public GameObject PlanetPointer
    {
        get
        {
            return fPlanetPointer;
        }

        set
        {
            fPlanetPointer = value;
            fPointPosition = fPlanetPointer.transform.position;
        }
    }
}

