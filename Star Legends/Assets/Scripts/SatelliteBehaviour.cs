﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SatelliteBehaviour : EnemyAbstract
{
    private bool fInPlanet;
    private Vector2 fPlanetPos;

	// Use this for initialization
	void Start ()
    {
        fAction = null;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (fInPlanet)
            StartCoroutine(Movement());
        else if (Vector2Distance(transform.position, GameController.gPlayer.transform.position) < fAggroRange)
            if (fAction == null)
            {
                fAudio.GetComponent<AudioController>().PlayerAudio("laser");
                fAction = StartCoroutine(Attack());
            }
        else
            transform.up = ((Vector2)transform.position - fPlanetPos).normalized;
    }

    protected override void Death()
    {
        Destroy(gameObject);
    }

    protected override IEnumerator Attack()
    {
        transform.up = (transform.position - GameController.gPlayer.transform.position).normalized;

        GameObject projectile = Instantiate(fBullet, transform.position, transform.rotation);
        projectile.transform.position = new Vector3(transform.position.x, transform.position.y, 0);
        projectile.GetComponent<Rigidbody2D>().AddForce(transform.up * fBulletSpeed * -1, ForceMode2D.Impulse);
        
        yield return new WaitForSeconds(fShootIntervil);

        fAction = null;

        yield break;
    }

    protected override IEnumerator Movement()
    {     
        transform.position = new Vector2(transform.position.x + 0.1f, transform.position.y);

        yield break;
    }

    private void OnTriggerEnter2D(Collider2D pCollision)
    {
        if (pCollision.tag == "Planet")
        {
            fInPlanet = true;
            fPlanetPos = pCollision.transform.position;
        }

        else if (pCollision.tag == "PlayerBullet")
        {
            StartCoroutine(Damage());
        }
    }

    private void OnTriggerExit2D(Collider2D pCollision)
    {
        if (pCollision.tag == "Planet")
        {
            fInPlanet = false;
            pCollision.gameObject.GetComponent<PlanetController>().AddSatilate(gameObject);       
        }
    }
}
