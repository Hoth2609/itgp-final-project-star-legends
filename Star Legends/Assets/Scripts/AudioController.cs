﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    [SerializeField]
    private AudioClip[] fLasers;
    [SerializeField]
    private AudioClip[] fExplosions;
    [SerializeField]
    private AudioClip[] fHits;
    [SerializeField]
    private AudioClip fHyperInto;
    [SerializeField]
    private AudioClip fHyperOut;
    [SerializeField]
    private AudioClip fHyperStay;
    [SerializeField]
    private AudioClip fEngine;
    [SerializeField]
    private AudioClip fMusic;
    [SerializeField]
    private AudioClip fUI;
    [SerializeField]
    private AudioClip fUI_select;

    private AudioSource fAudioSource;

    // Start is called before the first frame update
    void Awake()
    {
        fAudioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayerAudio(string pAudioType)
    {
        switch(pAudioType)
        {
            case "laser":
                fAudioSource.clip = fLasers[Random.Range(0, fLasers.Length)];
                fAudioSource.loop = false;
                fAudioSource.Play();
                break;
            case "explosion":
                fAudioSource.clip = fExplosions[Random.Range(0, fExplosions.Length)];
                fAudioSource.loop = false;
                fAudioSource.Play();
                break;
            case "hit":
                if (fAudioSource.clip != fHyperInto && !fAudioSource.isPlaying)
                {
                    fAudioSource.clip = fHits[Random.Range(0, fHits.Length)];
                    fAudioSource.loop = false;
                    fAudioSource.Play();
                }
                
                break;
            case "into_hyper":
                fAudioSource.clip = fHyperInto;
                fAudioSource.loop = false;
                fAudioSource.Play();
                break;
            case "out_hyper":
                fAudioSource.clip = fHyperOut;
                fAudioSource.loop = false;
                fAudioSource.Play();
                break;
            case "in_hyper":               
                if (!fAudioSource.isPlaying)
                {
                    fAudioSource.clip = fHyperStay;
                    fAudioSource.loop = true;
                    fAudioSource.Play();
                }
                
                break;

            case "music":
                if (!fAudioSource.isPlaying)
                {
                    fAudioSource.clip = fMusic;
                    fAudioSource.loop = true;
                    fAudioSource.Play();
                }

                break;

            case "engine":
                if (!fAudioSource.isPlaying)
                {
                    fAudioSource.clip = fEngine;
                    fAudioSource.loop = false;
                    fAudioSource.Play();
                }

                break;

            case "ui":
                fAudioSource.clip = fUI;
                fAudioSource.loop = false;
                fAudioSource.Play();
                break;

            case "ui_select":
                fAudioSource.clip = fUI_select;
                fAudioSource.loop = false;
                fAudioSource.Play();
                break;
        }
    }

    public void StopMusic()
    {
        if (fAudioSource != null)
            if (fAudioSource.isPlaying)
                fAudioSource.Stop();
    }
}
