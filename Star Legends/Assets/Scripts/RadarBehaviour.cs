﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadarBehaviour : MonoBehaviour
{
    private List<GameObject> fPointers;
    [SerializeField]
    private GameObject fPointerbase;

    private float fWidth;
    private float fHeight;
    private float fXPadding;
    private float fYPadding;

	// Use this for initialization
	void Awake()
    {
        fPointers = null;
    }
	
	// Update is called once per frame
	void Update()
    {
        if (fPointers != null)
        {
            foreach (GameObject lObject in fPointers)
            {
                Vector2 direction = (lObject.GetComponent<PointerInfo>().PointPosition - (Vector2)lObject.transform.position).normalized;
                Vector2 camPosition = Camera.main.transform.position;
                Vector2 iconPosition = camPosition + (direction * 20f);

                float camXMin = camPosition.x - (fWidth * 0.5f);
                float camXMax = camPosition.x + (fWidth * 0.5f);
                float camYMin = camPosition.y - (fHeight * 0.5f);
                float camYMax = camPosition.y + (fHeight * 0.5f);

                iconPosition.x = Mathf.Clamp(iconPosition.x, camXMin + fXPadding, camXMax - fXPadding);
                iconPosition.y = Mathf.Clamp(iconPosition.y, camYMin + fYPadding, camYMax - fYPadding);

                lObject.transform.up = direction;
                lObject.transform.position = iconPosition;
            }
        }    
    }

    public void GeneratePointers()
    {
        GameObject pointer;
        fPointers = new List<GameObject>();

        foreach (GameObject planet in GameController.gWorldPlanets)
        {
            pointer = Instantiate(fPointerbase);
            pointer.GetComponent<PointerInfo>().PlanetPointer = planet;
            fPointers.Add(pointer);
        }

        pointer = Instantiate(fPointerbase);
        pointer.GetComponent<PointerInfo>().PlanetPointer = GameController.gStar;
        fPointers.Add(pointer);

        float pixelHeight = Camera.main.pixelHeight;
        float pixelWidth = Camera.main.pixelWidth;

        Vector3 cameraPosition = Camera.main.transform.position;
        Vector3 bottomLeft = Camera.main.ScreenToWorldPoint(new Vector2(0f, 0f));
        Vector3 topRight = Camera.main.ScreenToWorldPoint(new Vector2(Camera.main.pixelWidth, Camera.main.pixelHeight));

        fWidth = Mathf.Abs(topRight.x - bottomLeft.x);
        fHeight = Mathf.Abs(topRight.y - bottomLeft.y);

        fXPadding = fWidth * 0.035f;
        fYPadding = fHeight * 0.035f;
    }

    public void DestroyPointers()
    {
        if (fPointers != null)
        {
            foreach (GameObject pointer in fPointers)
                Destroy(pointer);

            fPointers.Clear();
            //fPointers = null;     
        }
    }
}
