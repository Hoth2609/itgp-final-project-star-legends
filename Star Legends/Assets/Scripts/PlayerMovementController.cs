﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementController : MonoBehaviour
{
    [SerializeField]
    private int fForceApplied;
    [SerializeField]
    private int fMaxSpeed;
    [SerializeField]
    private Material[] fBackground;

    private bool fInLandingArea;
    private bool fIsEnabled;
    private GameObject fLanding;
    private GameObject fPlanetIn;
    private GameObject fGameController;
    private GameController fGc;
    private Rigidbody2D fRigidbody2D;
    private Coroutine fBeginingEnd;

    private int desperation;

    // Use this for initialization
    void Awake()
    {
        fRigidbody2D = GetComponent<Rigidbody2D>();
        fLanding = null;
        fIsEnabled = true;
        fGameController = GameObject.Find("GameController");
        fGc = fGameController.GetComponent<GameController>();
        fBeginingEnd = null;
        desperation = 0;
        gameObject.layer = 10;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameController.gIntroOrOutroPlay && fBeginingEnd == null)
        {
            if (Vector2Distance(GameController.gIntroAndOutroTarget.transform.position) > 775)
            {
                GetComponentInChildren<AudioController>().PlayerAudio("in_hyper");
                fRigidbody2D.AddForce(Seek(GameController.gIntroAndOutroTarget.transform.position));
            }

            else if (Vector2Distance(GameController.gIntroAndOutroTarget.transform.position) > 750)
            {
                GetComponentInChildren<AudioController>().PlayerAudio("out_hyper");
                gameObject.layer = 10;
                fRigidbody2D.AddForce(Seek(GameController.gIntroAndOutroTarget.transform.position));
            }

            else if (Vector2Distance(GameController.gIntroAndOutroTarget.transform.position) > 22)
                fRigidbody2D.AddForce(Seek(GameController.gIntroAndOutroTarget.transform.position));

            else if (GameController.gIntroAndOutroTarget == GameController.gStar)
            {
                GameController.gGameState = true;
                transform.position = new Vector3(0, -22, 0);
                ResetVelocity();
            }              

            else if (Vector2Distance(GameController.gIntroAndOutroTarget.transform.position) > 5 && GameController.gIntroAndOutroTarget == GameObject.Find("player_ship_landing"))
            {
                fRigidbody2D.AddForce(Seek(GameController.gIntroAndOutroTarget.transform.position));
                fRigidbody2D.velocity = Truncate(fRigidbody2D.velocity, 5);
                GetComponentInChildren<ShipBehaviourController>().gameObject.transform.up = (GameController.gIntroAndOutroTarget.transform.position - transform.position).normalized;
                GameController.EndGame();
                fGameController.GetComponentInChildren<AudioController>().PlayerAudio("music");
            }

            else if (GameController.gIntroAndOutroTarget == GameObject.Find("player_ship_landing"))
            {
                fRigidbody2D.AddForce(Seek(GameController.gIntroAndOutroTarget.transform.position));
                fRigidbody2D.velocity = Truncate(fRigidbody2D.velocity, 2);
                var dir = GameController.gIntroAndOutroTarget.transform.up * 2;
                var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
                GetComponentInChildren<ShipBehaviourController>().gameObject.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
            }

            else
            {
                transform.position = new Vector3(0, -22, 0);
                ResetVelocity();
            }
                                    

            fRigidbody2D.velocity = Truncate(fRigidbody2D.velocity, 300);
        }
           
        else if (GameController.gGameState)
        {
            if (Input.GetKeyDown(KeyCode.J))
            {
                GameController.gGameState = false;
                fBeginingEnd = StartCoroutine(OutroPlay());
                fGc.IntroAndOutro(false);
                fIsEnabled = true;
                GetComponentInChildren<ShipBehaviourController>().HasLanded = false;
            }

            else
            {
                if ((Input.GetKeyDown(KeyCode.W) || Input.GetKey(KeyCode.W)) && fIsEnabled)
                {
                    if (Input.GetKey(KeyCode.Space))
                        fRigidbody2D.AddForce(new Vector2(0, fForceApplied * 2), ForceMode2D.Force);
                    else
                        fRigidbody2D.AddForce(new Vector2(0, fForceApplied), ForceMode2D.Force);

                    fGameController.GetComponentInChildren<AudioController>().PlayerAudio("engine");
                }                    

                if ((Input.GetKeyDown(KeyCode.S) || Input.GetKey(KeyCode.S)) && fIsEnabled)
                {
                    if (Input.GetKey(KeyCode.Space))
                        fRigidbody2D.AddForce(new Vector2(0, fForceApplied * -2), ForceMode2D.Force);
                    else
                        fRigidbody2D.AddForce(new Vector2(0, fForceApplied * -1), ForceMode2D.Force);

                    fGameController.GetComponentInChildren<AudioController>().PlayerAudio("engine");
                }                    

                if ((Input.GetKeyDown(KeyCode.D) || Input.GetKey(KeyCode.D)) && fIsEnabled)
                {
                    if (Input.GetKey(KeyCode.Space))
                        fRigidbody2D.AddForce(new Vector2(fForceApplied * 2, 0), ForceMode2D.Force);
                    else
                        fRigidbody2D.AddForce(new Vector2(fForceApplied, 0), ForceMode2D.Force);

                    fGameController.GetComponentInChildren<AudioController>().PlayerAudio("engine");
                }

                if ((Input.GetKeyDown(KeyCode.A) || Input.GetKey(KeyCode.A)) && fIsEnabled)
                {
                    if (Input.GetKey(KeyCode.Space))
                        fRigidbody2D.AddForce(new Vector2(fForceApplied * -2, 0), ForceMode2D.Force);
                    else
                        fRigidbody2D.AddForce(new Vector2(fForceApplied * -1, 0), ForceMode2D.Force);

                    fGameController.GetComponentInChildren<AudioController>().PlayerAudio("engine");
                }          

                if (Input.GetKeyDown(KeyCode.L) && fInLandingArea)
                {
                    if (fIsEnabled)
                    {
                        fIsEnabled = false;
                        if (fPlanetIn != null && fPlanetIn.GetComponent<PlanetController>().ScoreMax > 0)
                        {
                            fGc.StartScore(fPlanetIn);
                            fPlanetIn.GetComponent<PlanetController>().ScoreMax--;
                        }

                        GetComponentInChildren<ShipBehaviourController>().HasLanded = true;
                    }

                    else
                    {
                        fIsEnabled = true;
                        GetComponentInChildren<ShipBehaviourController>().HasLanded = false;
                        fGc.StopScore();
                    }
                }

                if (!fIsEnabled)
                {
                    fRigidbody2D.velocity = new Vector2();
                    transform.position = Vector2.MoveTowards(transform.position, fLanding.transform.position, 1);
                }

                fRigidbody2D.velocity = Truncate(fRigidbody2D.velocity, fMaxSpeed);
            }          
        }

        else
        {
            GetComponentInChildren<ShipBehaviourController>().HasLanded = false;
            fIsEnabled = true;
        }      
    }

    private IEnumerator JUSTSTOPPLEASE()
    {
        yield return new WaitForSeconds(0.5f);

        ResetVelocity();

        yield break;
    }

    private IEnumerator OutroPlay()
    {
        GetComponentInChildren<AudioController>().PlayerAudio("into_hyper");
        fIsEnabled = true;

        yield return new WaitForSeconds(5.5f);
        
        if (!GameController.gGameEnded)
        {
            gameObject.layer = 15;
            SetPlayerBackgorundMesh(0);
            transform.position = new Vector3(transform.position.x, transform.position.y, -20);
            fBeginingEnd = null;
        }

        GetComponentInChildren<RadarBehaviour>().DestroyPointers();    

        yield break;
    }

    private void OnTriggerEnter2D(Collider2D pCollider)
    {
        if (pCollider.tag == "Star")
            GetComponentInChildren<ShipBehaviourController>().Damage();

        if (pCollider.tag == "EnemyBullet")
            GetComponentInChildren<ShipBehaviourController>().Damage();

        if (pCollider.tag == "LandingZone")
        {
            fInLandingArea = true;
            fLanding = pCollider.gameObject;
        }

        if (pCollider.tag == "Planet")
            fPlanetIn = pCollider.gameObject;
    }

    private void OnTriggerStay2D(Collider2D pCollider)
    {
        if (pCollider.tag == "Star" || pCollider.tag == "Enemy")
            GetComponentInChildren<ShipBehaviourController>().Damage();
    }

    private void OnTriggerExit2D(Collider2D pCollider)
    {
        if (pCollider.tag == "LandingZone")
        {
            fInLandingArea = false;
            fLanding = null;
        }

        if (pCollider.tag == "Planet")
            fPlanetIn = null;
    }

    private void OnCollisionEnter2D(Collision2D pCollision)
    {
        if (pCollision.gameObject.tag == "Asteriod")
            GetComponentInChildren<ShipBehaviourController>().Damage();
    }

    private Vector2 Truncate(Vector2 vec2, float maxLength)
    {
        if (Vector2Length(vec2) > maxLength)
        {
            vec2.Normalize();
            vec2 *= maxLength;
        }

        return vec2;
    }

    private float Vector2Length(Vector2 vec2)
    {
        return Mathf.Sqrt((vec2.x * vec2.x) + (vec2.y * vec2.y));
    }

    public void ResetVelocity()
    {
        //fRigidbody2D.isKinematic = true;
        fRigidbody2D.velocity = Vector2.zero;
        fRigidbody2D.angularVelocity = 0;

        if (desperation < 10)
            fRigidbody2D.velocity = Vector2.zero;
        else
            desperation = 0;
    }

    public Vector2 Seek(Vector2 pTarget)
    {
        Vector2 lTarget = pTarget;
        lTarget = (lTarget - (Vector2)transform.position) * 1;
        lTarget = lTarget - fRigidbody2D.velocity;

        return lTarget;
    }

    public float Vector2Distance(Vector2 vec2)
    {
        float dx = vec2.x - transform.position.x;
        float dy = vec2.y - transform.position.y;
        return Mathf.Sqrt(dx * dx + dy * dy);
    }

    public void SetPlayerBackgorundMesh(int pBackgorund)
    {
        MeshRenderer lPlayMesh = GetComponentInChildren<MeshRenderer>();
        lPlayMesh.material = fBackground[pBackgorund];
    }
}