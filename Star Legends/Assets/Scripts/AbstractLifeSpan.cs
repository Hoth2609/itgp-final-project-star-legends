﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractLifeSpan : MonoBehaviour
{
    [SerializeField]
    private int pLifeSpan;
    private int fLifeSpan;

    private Coroutine fCoLife;

    // Use this for initialization
    void Start ()
    {
        fLifeSpan = pLifeSpan;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (fCoLife == null)
        {
            StopAllCoroutines();
            fCoLife = StartCoroutine(ChangeLifeSpan());
        }
    }

    protected virtual IEnumerator ChangeLifeSpan()
    {
        fLifeSpan--;

        yield return new WaitForSeconds(1);

        fCoLife = null;

        //Debug.Log(fLifeSpan);
        if (fLifeSpan <= 0)
            GameObject.Destroy(gameObject);
    }
}
