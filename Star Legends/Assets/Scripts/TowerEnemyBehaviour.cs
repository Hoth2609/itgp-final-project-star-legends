﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerEnemyBehaviour : EnemyAbstract
{

	// Use this for initialization
	private void Awake()
    {
        fTroopsAvailable = new List<GameObject>();
        myRigidbody = GetComponent<Rigidbody2D>();
    }
	
	// Update is called once per frame
	private void Update ()
    {
        if (fAction == null)
        {
            StopAllCoroutines();

            if (Vector2Distance(GameController.gPlayer.transform.position, transform.position) < 20)
            {
                fAudio.GetComponent<AudioController>().PlayerAudio("laser");
                fAction = StartCoroutine(Attack());
            }  
        }
    }

    protected override IEnumerator Attack()
    {
        Vector2 toPlayer = GameController.gPlayer.transform.position - transform.position;
        float lookAheadTime = Vector2Length(toPlayer) / (fBulletSpeed + Vector2Length(GameController.gPlayer.GetComponent<Rigidbody2D>().velocity));
        Vector2 lookAheadPos = (Vector2)GameController.gPlayer.transform.position + GameController.gPlayer.GetComponent<Rigidbody2D>().velocity * lookAheadTime;
        Vector2 lLookVec = (lookAheadPos - (Vector2)transform.position).normalized;

        GameObject projectile = Instantiate(fBullet);
        projectile.transform.right = (lLookVec + new Vector2(Random.Range(-0.5f, 0.5f), Random.Range(-0.5f, 0.5f))).normalized;
        projectile.transform.position = new Vector3(transform.position.x, transform.position.y, 0);
        projectile.GetComponent<Rigidbody2D>().AddForce(projectile.transform.up * fBulletSpeed * 1, ForceMode2D.Impulse);

        yield return new WaitForSeconds(fShootIntervil);

        fAction = null;

        yield break;
    }

    protected override void Death()
    {
        // play death animation
        // delete
        Destroy(gameObject);
    }

    protected void OnTriggerEnter2D(Collider2D pCollision)
    {
        if (pCollision.gameObject.tag == "PlayerBullet")
        {
            fAudio.GetComponent<AudioController>().PlayerAudio("hit");
            StartCoroutine(Damage());
        }
    }
}
