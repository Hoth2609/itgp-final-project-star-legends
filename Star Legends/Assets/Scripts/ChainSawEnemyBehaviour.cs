﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChainSawEnemyBehaviour : EnemyAbstract
{

	// Use this for initialization
	private void Awake ()
    {
        fTroopsAvailable = new List<GameObject>();
        myRigidbody = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	private void Update ()
    {
        if (fAction == null)
        {
            StopAllCoroutines();
            fAction = StartCoroutine(Movement());
        }
    }

    protected override void Death()
    {
        // play death animation
        // delete     
        Destroy(gameObject);
    }

    protected override IEnumerator Attack()
    {
        yield break;
    }

    private void OnCollisionEnter2D(Collision2D pCollision)
    {
        if (pCollision.gameObject.tag == "Player")
        {           
            pCollision.gameObject.GetComponentInChildren<ShipBehaviourController>().Damage();
        }
            
    }

    protected void OnTriggerEnter2D(Collider2D pCollision)
    {
        if (pCollision.tag == "PlayerBullet")
        {
            fAudio.GetComponent<AudioController>().PlayerAudio("hit");
            StartCoroutine(Damage());
        }
    }

    private void OnCollisionStay2D(Collision2D pCollision)
    {
        if (pCollision.gameObject.tag == "Player")
        {
            pCollision.gameObject.GetComponentInChildren<ShipBehaviourController>().Damage();
        }
            
    }
}
