﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFighterBehaviour : EnemyAbstract
{

	// Use this for initialization
	private void Awake()
    {
        fTroopsAvailable = new List<GameObject>();
        myRigidbody = GetComponent<Rigidbody2D>();
    }
	
	// Update is called once per frame
	private void Update()
    {
        //Debug.Log(Vector2Distance(GameController.gPlayer.transform.position, transform.position));
        if (Vector2Distance(GameController.gPlayer.transform.position, transform.position) <= 10)
        {
            Vector2 toPlayer = GameController.gPlayer.transform.position - transform.position;
            float lookAheadTime = Vector2Length(toPlayer) / (fBulletSpeed + Vector2Length(GameController.gPlayer.GetComponent<Rigidbody2D>().velocity));
            Vector2 lookAheadPos = (Vector2)GameController.gPlayer.transform.position + GameController.gPlayer.GetComponent<Rigidbody2D>().velocity * lookAheadTime;
            transform.up = (lookAheadPos - (Vector2)transform.position).normalized;
        }
        
        if (fAction == null)
        {
            StopAllCoroutines();

            if (Vector2Distance(GameController.gPlayer.transform.position, transform.position) > 10)
            {
                //Debug.Log("Start");
                fAction = StartCoroutine(Movement());
            }
                
            else
            {
                fAudio.GetComponent<AudioController>().PlayerAudio("laser");
                fAction = StartCoroutine(Attack());
            }       
        }
    }

    protected override IEnumerator Attack()
    {
        GameObject projectile = Instantiate(fBullet);
        projectile.transform.up = (transform.up + new Vector3(Random.Range(-0.5f, 0.5f), Random.Range(-0.5f, 0.5f))).normalized;
        projectile.transform.position = new Vector3(transform.position.x, transform.position.y, 0); 
        projectile.GetComponent<Rigidbody2D>().AddForce(projectile.transform.up * fBulletSpeed * 1, ForceMode2D.Impulse);

        yield return new WaitForSeconds(fShootIntervil);

        fAction = null;

        yield break;
    }

    protected override void Death()
    {
        // play death animation
        // delete
        Destroy(gameObject);
    }

    protected void OnTriggerEnter2D(Collider2D pCollision)
    {
        if (pCollision.gameObject.tag == "PlayerBullet")
        {
            fAudio.GetComponent<AudioController>().PlayerAudio("hit");
            StartCoroutine(Damage());
        }
    }
}
