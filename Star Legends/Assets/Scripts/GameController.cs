﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    
    [SerializeField]
    private int fAsteriodSpawnMin;
    [SerializeField]
    private int fAsteriodSpawnMax;
    [SerializeField]
    private int fGalaxyRadius;
    [SerializeField]
    private int fPlanetNumber;
    [SerializeField]
    private int fEnemySpawnRateMin;
    [SerializeField]
    private int fEnemySpawnRateMax;
    [SerializeField]
    private GameObject fAsteriod;
    [SerializeField]
    private GameObject fPlayerBase;
    [SerializeField]
    private GameObject[] fStars;
    [SerializeField]
    private GameObject[] fPlanets;
    [SerializeField]
    private GameObject[] fEnemies;
    [SerializeField]
    private GameObject[] fEnemyTroops;
    [SerializeField]
    private GameObject fPlayer;
    [SerializeField]
    private GameObject fScoreText;
    [SerializeField]
    private GameObject[] fEndScreenUI;
    [SerializeField]
    private GameObject fLivesText;
    

    private int fScore;
    private int fMaxScore;
    private int fScoreMultiplier;
    private bool fIsScoring;

    private Dictionary<GameObject, Vector2> fPlanetAndLandZone;
    private GameObject fWorldStar;
    private GameObject fScorePlanet;
    private Coroutine fUpdateScore;
    private Coroutine fSpawnAsteriods;
    private Coroutine fSpawnEnemies;

    public static GameObject gPlayer;
    public static GameObject gStar;
    public static GameObject gIntroAndOutroTarget;
    public static List<GameObject> gWorldPlanets;
    public static bool gGameState;
    public static bool gIntroOrOutroPlay;
    public static bool gGameEnded;
    

    private void Awake()
    {
        fScore = 0;
        fIsScoring = false;
        gPlayer = fPlayer;
        gIntroOrOutroPlay = true;
        gIntroAndOutroTarget = null;
        gWorldPlanets = new List<GameObject>();
        fScorePlanet = new GameObject();
        gGameEnded = false;

        ResetWorld();
        //GenerateWorld();
        fScoreMultiplier = 1;
        gGameState = true;
    }

    private void Update()
    {     
        PlayerMovementController lPlayMovement = gPlayer.GetComponent<PlayerMovementController>();

        if (lPlayMovement.Vector2Distance(gIntroAndOutroTarget.transform.position) > 22 && gIntroOrOutroPlay)
        {
            gIntroOrOutroPlay = true;
            gGameState = false;
        }           

        else if (gIntroOrOutroPlay && gIntroAndOutroTarget == gStar)
        {         
            gPlayer.transform.position = new Vector3(gPlayer.transform.position.x, gPlayer.transform.position.y, 0);
            gPlayer.GetComponent<PlayerMovementController>().SetPlayerBackgorundMesh(1);

            Camera.main.GetComponent<RadarBehaviour>().GeneratePointers();

            gGameState = true;
            gIntroOrOutroPlay = false;
        }
        
        else if (gIntroOrOutroPlay && gIntroAndOutroTarget == fPlayerBase)
        {
            gPlayer.transform.position = new Vector3(gPlayer.transform.position.x, gPlayer.transform.position.y, 0);
            gPlayer.GetComponent<PlayerMovementController>().SetPlayerBackgorundMesh(1);
        }
        
        else if (gGameState)
        {
            if (fIsScoring && fUpdateScore == null && fScorePlanet != null)
            {
                if (fScorePlanet.GetComponent<PlanetController>().ScoreMax > 0)
                {
                    fUpdateScore = StartCoroutine(UpdateScore());
                    fScorePlanet.GetComponent<PlanetController>().ScoreMax--;
                }

                else
                    fUpdateScore = null;
            }

            if (fSpawnAsteriods == null)
                fSpawnAsteriods = StartCoroutine(SpawnAsteriods());


            if (fSpawnEnemies == null)
                fSpawnEnemies = StartCoroutine(SpawnEnemies());
        }      
    }

    public void StartScore(GameObject pPlanet)
    {
        fIsScoring = true;
        fScorePlanet = pPlanet;
        fScoreMultiplier = pPlanet.GetComponent<PlanetController>().ScoreMax;
    }

    public void StopScore()
    {
        if (fUpdateScore != null)
        {
            StopAllCoroutines();
            fUpdateScore = null;
        }

        fIsScoring = false;
    }

    private IEnumerator UpdateScore()
    {
        fScore += 1 * fScoreMultiplier;

        GetComponent<AudioController>().PlayerAudio("hit");

        fScoreText.GetComponent<Text>().text = fScore.ToString();

    yield return new WaitForSeconds(1);

        fUpdateScore = null;
        yield break;
    }

    private IEnumerator SpawnEnemies()
    {
        yield return new WaitForSeconds(Random.Range(fEnemySpawnRateMin, fEnemySpawnRateMax));

        GameObject enemy = Instantiate(fEnemies[Random.Range(0, fEnemies.Length)]);
        enemy.transform.position = new Vector2(Random.Range(fGalaxyRadius * -1, fGalaxyRadius + 1), Random.Range(fGalaxyRadius * -1, fGalaxyRadius + 1));

        switch (Random.Range(0, 4))
        {
            case 0:
                GameObject enemyTroop = fEnemyTroops[Random.Range(0, fEnemyTroops.Length)];
                enemy.GetComponent<EnemyAbstract>().AddTroop(enemyTroop);
                break;
            default:
                break; 
        }

        fSpawnEnemies = null;
        yield break;
    }

    private IEnumerator SpawnAsteriods()
    {
        yield return new WaitForSeconds(Random.Range(fAsteriodSpawnMin, fAsteriodSpawnMax));

        List<int> lIntList = new List<int>() { -20, 20 };

        int lRandomX = lIntList[Random.Range(0, 2)];
        int lRandomY = lIntList[Random.Range(0, 2)];

        int x = 0;
        int y = 0;

        if (lRandomX > 0)
            x = Screen.width + lRandomX;

        else
            x = 0 + lRandomX;

        if (lRandomY > 0)
            y = Screen.height + lRandomY;       

        else
            y = 0 + lRandomX;

        Vector3 vec3 = Camera.main.ScreenToWorldPoint(new Vector3(x, y));
        vec3.z = 0;

        GameObject asteriod = Instantiate(fAsteriod, vec3, transform.rotation);

        int lTempSize = Random.Range(0, 3);

        asteriod.GetComponent<AsteriodBehaviour>().SetSize(lTempSize);
        asteriod.GetComponent<AsteriodBehaviour>().HitPlayer();

        fSpawnAsteriods = null;
        yield break;
    }

    public void GenerateWorld()
    {
        fScore = 0;
        fScoreText.GetComponent<Text>().text = fScore.ToString();

        // create star
        GameObject star = Instantiate(fStars[Random.Range(0, fStars.Length)]);

        foreach (GameObject gUIElement in fEndScreenUI)
            gUIElement.SetActive(false);

        star.transform.position = new Vector2(0, 0);
        gStar = star;

        for (int i = 0; i < fPlanetNumber; i++)
        {
            // create planet
            GameObject planet = Instantiate(fPlanets[Random.Range(0, fPlanets.Length)]);
            // change position of planet
            planet.transform.position = new Vector2(Random.Range(fGalaxyRadius * -1, fGalaxyRadius + 1), Random.Range(fGalaxyRadius * -1, fGalaxyRadius + 1));
            // put in list
            gWorldPlanets.Add(planet);
        }

        gGameEnded = false;
        fSpawnEnemies = null;
        IntroAndOutro(true);

        GetComponent<AudioController>().StopMusic();
    }

    public void UpdateLives(int pLives)
    {
        fLivesText.GetComponent<Text>().text = "x" + pLives.ToString();
    }

    public static void ResetWorld()
    {
        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Enemy"))
            if (obj != null)
                Destroy(obj);

        Camera.main.GetComponent<RadarBehaviour>().DestroyPointers();

        gWorldPlanets.Clear();

        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Planet"))
            if (obj != null)
                Destroy(obj);

        gStar = null;

        Destroy(GameObject.FindGameObjectWithTag("Star"));

        gPlayer.GetComponentInChildren<ShipBehaviourController>().Lives = 3;

        gPlayer.GetComponentInChildren<ShipBehaviourController>().gameObject.GetComponent<SpriteRenderer>().enabled = true;
        gPlayer.transform.position = new Vector3(0, -5000, -20);
        gPlayer.GetComponent<PlayerMovementController>().SetPlayerBackgorundMesh(0);

        GameObject.Find("GameController").GetComponent<GameController>().GenerateWorld();
    }

    public void IntroAndOutro(bool pIsIntro)
    {     
        if (pIsIntro)
            gIntroAndOutroTarget = gStar;

        else        
            gIntroAndOutroTarget = fPlayerBase;

        gIntroOrOutroPlay = true;       
    }

    public static void EndGame()
    {
        GameObject GC = GameObject.Find("GameController");
        GameController lGC = GC.GetComponent<GameController>();

        foreach (GameObject gUIElement in lGC.fEndScreenUI)
            gUIElement.SetActive(true);

        if (lGC.fScore > lGC.fMaxScore)
            lGC.fMaxScore = lGC.fScore;

        lGC.fEndScreenUI[0].GetComponent<Text>().text = lGC.MaxScore.ToString();

        gGameEnded = true;
        gGameState = false;
    }

    public int Score
    {
        get
        {
            return fScore;
        }

        set
        {
            fScore = value;
            fScoreText.GetComponent<Text>().text = fScore.ToString();
        }
    }

    public int MaxScore
    {
        get
        {
            return fMaxScore;
        }

        set
        {
            fMaxScore = value;
        }
    }
}
