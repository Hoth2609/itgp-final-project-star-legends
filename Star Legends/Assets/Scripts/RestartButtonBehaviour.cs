﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartButtonBehaviour : MonoBehaviour
{
    [SerializeField]
    private GameObject fGameController;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ResetGame()
    {
        GameController.ResetWorld();
    }

    public void ToMainMenu()
    {
        SceneManager.LoadScene("Menu_Scene");
    }

    public void PlayEnterSound()
    {
        GameController.gPlayer.GetComponentInChildren<AudioController>().PlayerAudio("ui");
    }

    public void PlayerSelectSound()
    {
        GameController.gPlayer.GetComponentInChildren<AudioController>().PlayerAudio("ui_select");
    }
}
