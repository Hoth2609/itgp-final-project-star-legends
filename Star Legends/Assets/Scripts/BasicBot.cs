﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicBot : MonoBehaviour
{
    private Rigidbody2D myRigidbody;
    private Vector2 fWanderTarget;

    public int fWanderJitter;
    public int fWanderRadius;
    public int fWanderDist;
    public int fMaxSpeed;
    public int fMaxForce;
    public GameObject thing;
	// Use this for initialization
	void Start ()
    {
        myRigidbody = GetComponent<Rigidbody2D>();
        fWanderTarget = new Vector2(0, 0);
	}
	
	// Update is called once per frame
	void Update ()
    {
        Vector2 force = Compute();
        force = Truncate(force, fMaxForce);
        myRigidbody.AddForce(force);
        myRigidbody.velocity = Truncate(myRigidbody.velocity, fMaxSpeed);
    }

    private Vector2 Compute()
    {
        // wander
        Vector2 lTarget = fWanderTarget;

        int x = Random.Range(-1, 2);
        int y = Random.Range(-1, 2);

        lTarget += new Vector2(x, y);
        lTarget.Normalize();

        lTarget *= fWanderRadius;

        thing.transform.position = lTarget;

        // seek
        return (Seek(lTarget));
    }

    private Vector2 Truncate(Vector2 vec2, float maxLength)
    {
        if (Vector2Length(vec2) > maxLength)
        {
            vec2.Normalize();
            vec2 *= maxLength;
        }

        return vec2;
    }

    private float Vector2Length(Vector2 vec2)
    {
        //Debug.Log("Length" + (Mathf.Sqrt((vec2.x * vec2.x) + (vec2.y * vec2.y))));
        return Mathf.Sqrt((vec2.x * vec2.x) + (vec2.y * vec2.y));
    }

    private Vector2 Seek(Vector2 pTarget)
    {
        // seek
        Vector2 lTarget = pTarget;
        lTarget = (lTarget - new Vector2(transform.position.x, transform.position.y)) * fMaxSpeed;
        lTarget = lTarget - myRigidbody.velocity;
        //Debug.Log(lTarget);

        return lTarget;
    }
}
