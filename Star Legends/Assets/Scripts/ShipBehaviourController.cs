﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipBehaviourController : MonoBehaviour
{
    [SerializeField]
    private float fBulletSpeed;
    [SerializeField]
    private float fBulletIntervel;
    [SerializeField]
    private float fShipCatch;
    [SerializeField]
    private int fLives;
    [SerializeField]
    private GameObject fBullet;
    [SerializeField]
    private GameObject fMuzzle;
    [SerializeField]
    private GameObject fAudio;

    private Vector2 fAim;
    private Coroutine fDamaged;
    private Coroutine fShooting;

    private bool fHasLanded;

    // Use this for initialization
    void Awake()
    {
        fHasLanded = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameController.gIntroOrOutroPlay)
            transform.up = ((Vector2)GameController.gIntroAndOutroTarget.transform.position - (Vector2)transform.position).normalized;

        else if (!HasLanded && GameController.gGameState)
        {
            Vector2 mousePos = Input.mousePosition;
            mousePos = Camera.main.ScreenToWorldPoint(mousePos);

            //Vector2 direction = new Vector2(fMousePointer.transform.position.x - transform.position.x, fMousePointer.transform.position.y - transform.position.y);
            Vector2 direction = new Vector2(Mathf.Lerp(transform.up.x, mousePos.x - transform.position.x, 0.015f), Mathf.Lerp(transform.up.y, mousePos.y - transform.position.y, fShipCatch));

            transform.up = direction;
            fAim = direction.normalized;

            if (Input.GetMouseButtonDown(0))
            {
                fShooting = StartCoroutine(Shoot());
            }

            if (Input.GetMouseButtonUp(0))
            {
                if (fShooting != null)
                    StopCoroutine(fShooting);
            }
        }
    }

    private IEnumerator Shoot()
    {
        if (fDamaged == null && GameController.gGameState)
        {
            while (true)
            {
                GameObject projectile = Instantiate(fBullet, fMuzzle.transform.position, transform.rotation);
                projectile.GetComponent<Rigidbody2D>().velocity = fAim * fBulletSpeed;
                projectile.transform.position = new Vector3(projectile.transform.position.x, projectile.transform.position.y, 0);
                fAudio.GetComponent<AudioController>().PlayerAudio("laser");
                yield return new WaitForSeconds(fBulletIntervel);
            }
        }      
    }

    private void OnTriggerEnter2D(Collider2D pCollision)
    {
        if (pCollision.tag == "EnemyBullet")
            Damage();
    }

    private void OnCollisionEnter2D(Collision2D pCollision)
    {
        if (pCollision.gameObject.tag == "Asteriod")        
            Damage();          
    }

    public void Damage()
    {
        if (fDamaged == null && !GameController.gGameEnded)
        {
            fAudio.GetComponent<AudioController>().PlayerAudio("hit");
            fDamaged = StartCoroutine(DamagePlayer());     
        }
            
    }

    private IEnumerator DamagePlayer()
    {
        fLives--;
        GameObject.Find("GameController").GetComponent<GameController>().UpdateLives(fLives);

        if (fLives <= 0)
        {
            GetComponent<SpriteRenderer>().enabled = false;
            fAudio.GetComponent<AudioController>().PlayerAudio("explosion");
            GameObject.Find("GameController").GetComponent<GameController>().Score = 0;
            GameController.gGameState = false;
            GameController.EndGame();
        }

        else
        {
            GetComponent<SpriteRenderer>().enabled = false;

            fAudio.GetComponent<AudioController>().PlayerAudio("hit");

            yield return new WaitForSeconds(0.5f);

            GetComponent<SpriteRenderer>().enabled = true;

            yield return new WaitForSeconds(0.5f);

            GetComponent<SpriteRenderer>().enabled = false;

            yield return new WaitForSeconds(0.5f);

            GetComponent<SpriteRenderer>().enabled = true;
        }

        fDamaged = null;
        yield break;
    }

    public bool HasLanded
    {
        get
        {
            return fHasLanded;
        }

        set
        {
            fHasLanded = value;
        }
    }

    public int Lives
    {
        get
        {
            return fLives;
        }

        set
        {
            fLives = value;
            GameObject.Find("GameController").GetComponent<GameController>().UpdateLives(fLives);
        }
    }
}