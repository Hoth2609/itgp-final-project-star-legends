﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetController : MonoBehaviour
{
    private int fScore;
    private int fEnemyPresence;
    private float fPlanetDiameter;
    private float fPlanetRadius;
    private bool fHasSatillite;
    private Vector2 fLandingArea;   

	// Use this for initialization
	private void Awake()
    {
        fPlanetDiameter = GetComponent<SpriteRenderer>().size.x;
        fPlanetRadius = fPlanetDiameter / 4;
        fScore = UnityEngine.Random.Range(1, 11);
	}

    private void OnTriggerEnter2D(Collider2D pCollision)
    {
        if (pCollision.tag == "Enemy")
            fEnemyPresence++;
    }

    private void OnTriggerExit2D(Collider2D pCollision)
    {
        if (pCollision.tag == "Enemy")
            fEnemyPresence--;
    }

    public int EnemyPresence
    {
        get
        {
            return fEnemyPresence;
        }
    }

    public float PlanetRadius
    {
        get
        {
            return fPlanetRadius;
        }
    }

    public bool HasSatillite
    {
        get
        {
            return fHasSatillite;
        }
    }

    public int ScoreMax
    {
        get
        {
            return fScore;
        }

        set
        {
            fScore = value;

            if (fScore < 1)
                fScore = 1;
        }
    }

    public void AddSatilate(GameObject satillite)
    {
        try
        {
            Destroy(GetComponent<HingeJoint2D>());
        }
            
        catch (Exception){}

        HingeJoint2D hinge = gameObject.AddComponent<HingeJoint2D>();    
        hinge.connectedBody = satillite.GetComponent<Rigidbody2D>();
        
        JointMotor2D motor =  new JointMotor2D();
        motor.motorSpeed = 5;
        motor.maxMotorTorque = 10;
        hinge.motor = motor;
        hinge.useMotor = true;

        fHasSatillite = true;
    }
}
